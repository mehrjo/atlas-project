# Project plan: Image classification on Huawei Atlas

team memebers: Amir Dellali, Jonas Mehr

## Overview

The goal of this project is to run at least one existing image 
classification model on both a general purpose CPU and on the 
Huawei Atlas. We want to compare the performance of the two devices,
possibly across multiple models.

We will also try to build a (small) neural network ourselves to solve 
an image classification task. To keep the training manageable this model 
will only have a handful of categories.

If we still have time in the end, one option would be to get an online
classification running using the camera module.

## Milestones

- Existing model running on laptop
- Existing model running on Atlas
- Comparison of performance metrics accross platforms
- Own model built and trained on custom data
- Own model running on Atlas
- Comparison of performance metrics accross platforms
- (If enough time available) Classification based on camera input

## Deliverables
- Code
- Performance report

## Materials
- Atlas 200
- (Maybe) Camera modulde

## Risks
### - Technical issue with Atlas 200:
- Description: A technical issue with the Atlas 200 appears that is outside of our technical ability to fix.
- Mitigation: Ask for advice from TAs, and if problem is specific to device, get another.
### - Model does not converge on custom data:
- Description: Due to properties inherent to the data used to train the custom model, the model doesn't converge.
- Mitigation: See whether model converges with known and trusted dataset. If issue persists, change the model until it converges on the known data, then try again on the custom dataset. If it still doesn't converge, use different data, or, in the worst case scenario, proceed with an already existing dataset.
### - Training of model fails on laptop:
- Description: Due to technical limitations of either participant's laptop, the training cannot be conducted on either laptop (OOM issues).
- Mitigation: Either work around the problem (e.g. using a different batch size in case of OOM errors), or try training the model on a PC with a GPU or Google Colab.
### - (If enough time available) Model works on provided images but not on camera input:
- Description: For whatever reason, the model works on the provided training and testing images, but does not produce sensible data on camera inputs.
- Mitigation: Check for discrepancies in image processing between the provided images and the camera input (image compression, color scheme, dimensions, etc.) and fix them if necessary. If issues persist, try running existing model on camera input (e.g. CIFAR-10) and check whether that produces sensible data. If not, examine the frames from the camera to the model - something might be wrong with them. If however, the model returns largely correct labels in that case, try to tell whether there is a pattern between misclassifications, or in the worst case, abandon this additional part.

## Timetable

when | what|
---|---|
Week 1 | Find model, run and test it on laptop |
Week 2 | Convert model, run and test on atlas, find training dataset for own model |
Week 3 | Build and train own model |
Week 4 | Convert own model, run it on atlas |
Week 5 | Reserve, potentially deploy another model or use camera |
Week 6 | Prepare presentation |
