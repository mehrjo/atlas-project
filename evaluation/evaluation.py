import numpy as np
import matplotlib.pyplot as plt

def evaluate(times_laptop, times_atlas, model):
    laptop_avg = np.mean(times_laptop)*1000
    atlas_avg = np.mean(times_atlas)*1000
    laptop_std = np.std(times_laptop)*1000
    atlas_std = np.std(times_atlas)*1000
    print("evluation of " + model)
    print(f"laptop: {laptop_avg:.3f}ms +/- {laptop_std:.3f}ms")
    print(f"atlas: {atlas_avg:.3f}ms +/- {atlas_std:.3f}ms")
    print(f"speedup on atlas: {laptop_avg/atlas_avg:.2f}x\n")
    ind = np.arange(2)
    plt.bar(ind, [laptop_avg, atlas_avg], yerr=[laptop_std, atlas_std], width=0.3, log=False)
    plt.title('comparison of times for ' + model)
    plt.ylabel('time [ms]')
    plt.xticks(ind, ('laptop', 'atlas'))
    plt.show()


vgg_laptop = np.genfromtxt("VGG19_times_laptop.csv")
vgg_atlas = np.genfromtxt("VGG19_times_atlas.csv")

evaluate(vgg_laptop, vgg_atlas, 'VGG19')


own_laptop = np.genfromtxt("own_model_times_laptop.csv")
own_atlas = np.genfromtxt("own_model_times_atlas.csv")

evaluate(own_laptop, own_atlas, 'own model')

