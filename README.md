# Build instructions

```
cd build
docker build -t jupyterlab ./
docker run -p 8888:8888 jupyterlab
```

Visit localhost link (127.0.0.1:8888:?token=) presented in terminal to execute notebook.